# 1. Etablir un Diagramme de classe du modèle en appliquant les design patterns appropriés en justifiant les designs patterns appliqués.





## Screenshots

![Screenshot](https://i.imgur.com/SpLIL3u.png)



Nous avons utilisé deux patrons de conception dans cette activité, le premier est le patron de conception de stratégie « Strategy pattern », puisque nous avons besoin d’une méthode « traiter » qui permet de traiter le contenu du dessin en utilisant une famille d'algorithmes qui sont interchangeables dynamiquement.
Le second est le modèle de conception observable, car nous avons besoin d'un moyen d'informer les abonnés des modifications apportées.

# 2. Faire une implémentation du modèle en utilisant un projet Maven sans prendre en considération des aspects techniques.

https://gitlab.com/yons101/test-blanc-coo-et-design-pattern

# 3. Effectuer des Tests du modèle

```java
public class TestSerialize {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        boolean answer;
        do
        {
            answer = false;
            System.out.println("Serialize => Type 1 \t Deserialize => Type 2");
            switch (sc.nextInt()) {
                case 1:
                    Dessin dessin1 = new Dessin();
                    dessin1.add(new Cercle(new Point(1, 1), 55));
                    dessin1.add(new Cercle(new Point(2, 2), 66));
                    dessin1.add(new Rectangle(new Point(3, 3), 66, 66));
                    Group g1 = (Group) dessin1.add(new Group());
                    g1.addFigure(new Rectangle(new Point(4, 4), 22, 22));
                    g1.addFigure(new Cercle(new Point(5, 5), 44));
                    dessin1.dessiner();
                    dessin1.setStrategy(new StrategyImpl1());
                    dessin1.applyStrategy();
                    dessin1.setStrategy(new StrategyImpl2());
                    dessin1.applyStrategy();
                    dessin1.serialize("file.ser");
                    break;
                case 2:
                    Dessin dessin2 = Dessin.deserialize("file.ser");
                    dessin2.dessiner();
                    dessin2.setStrategy(new StrategyImpl1());
                    dessin2.applyStrategy();
                    dessin2.setStrategy(new StrategyImpl2());
                    dessin2.applyStrategy();
                    break;
                default:
                    System.out.println("choose from 1 to 2");
                    answer = true;
            }
        } while (answer);
    }
}
```

![Screenshot](https://i.imgur.com/BtoZLcC.png)

