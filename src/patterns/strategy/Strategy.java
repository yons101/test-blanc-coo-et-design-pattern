package patterns.strategy;

import shapes.Figure;

import java.util.List;

public interface Strategy {
    public void traiter(List<Figure> figures);
}
