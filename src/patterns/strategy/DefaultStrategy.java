package patterns.strategy;

import shapes.Figure;

import java.util.List;

public class DefaultStrategy implements Strategy {
    @Override
    public void traiter(List<Figure> figures) {
        for (Figure figure : figures) {
            System.out.println("Default Strategy");
            figure.setEpaisseur(1);
            figure.setCouleurContour(1);
            figure.setCouleurRemplissage(1);
            figure.dessiner();
        }
    }
}
