package patterns.strategy;

import shapes.Figure;
import shapes.Group;

import java.util.List;

public class StrategyImpl1 implements Strategy {
    @Override
    public void traiter(List<Figure> figures) {
        for (Figure figure : figures) {
            System.out.println("Algorithme 1");
            figure.setCouleurContour(2);
            figure.setCouleurRemplissage(2);
            figure.setEpaisseur(2);
            figure.dessiner();
        }
    }
}
