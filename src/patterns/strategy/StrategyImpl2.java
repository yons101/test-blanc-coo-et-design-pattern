package patterns.strategy;

import shapes.Figure;
import shapes.Group;

import java.util.List;

public class StrategyImpl2 implements Strategy {
    @Override
    public void traiter(List<Figure> figures) {
        for (Figure figure : figures) {
            System.out.println("Algorithme 2");
            figure.setCouleurContour(3);
            figure.setCouleurRemplissage(3);
            figure.setEpaisseur(3);
            figure.dessiner();
        }

    }
}
