package shapes;

import java.util.ArrayList;
import java.util.List;

public class Group extends Figure {
    private List<Figure> figures = new ArrayList<>();

    public Figure addFigure(Figure figure) {
        figures.add(figure);
        return figure;
    }


    @Override
    public double getSurface() {
        return 0;
    }

    @Override
    public double getPerimetre() {
        return 0;
    }

    @Override
    public void dessiner() {
        for (Figure figure : figures) {
            figure.dessiner();
        }
    }

    public List<Figure> getFigures() {
        return figures;
    }
}
