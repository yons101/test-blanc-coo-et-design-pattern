import patterns.strategy.StrategyImpl1;
import patterns.strategy.StrategyImpl2;
import shapes.*;

import java.util.Scanner;

public class TestSerialize {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        boolean answer;
        do
        {
            answer = false;
            System.out.println("Serialize => Type 1 \t Deserialize => Type 2");
            switch (sc.nextInt()) {
                case 1:
                    Dessin dessin1 = new Dessin();
                    dessin1.add(new Cercle(new Point(1, 1), 55));
                    dessin1.add(new Cercle(new Point(2, 2), 66));
                    dessin1.add(new Rectangle(new Point(3, 3), 66, 66));
                    Group g1 = (Group) dessin1.add(new Group());
                    g1.addFigure(new Rectangle(new Point(4, 4), 22, 22));
                    g1.addFigure(new Cercle(new Point(5, 5), 44));
                    dessin1.dessiner();
                    dessin1.setStrategy(new StrategyImpl1());
                    dessin1.applyStrategy();
                    dessin1.setStrategy(new StrategyImpl2());
                    dessin1.applyStrategy();
                    dessin1.serialize("file.ser");
                    break;
                case 2:
                    Dessin dessin2 = Dessin.deserialize("file.ser");
                    dessin2.dessiner();
                    dessin2.setStrategy(new StrategyImpl1());
                    dessin2.applyStrategy();
                    dessin2.setStrategy(new StrategyImpl2());
                    dessin2.applyStrategy();
                    break;
                default:
                    System.out.println("choose from 1 to 2");
                    answer = true;
            }
        } while (answer);
    }
}
